
# Lab #3 - SPI - I/O

## By C2C Bret Wagner

## Table of Contents 
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Preliminary Design](#preliminary-design)
 * [Pseudocode](#pseudocode)
5. [Code](#code)
6. [Debugging](#debugging)
7. [Testing methodology or results](#testing-methodology-or-results)
8. [Answers to Lab Questions](#answers-to-lab-questions)
8. [Observations and Conclusions](#observations-and-conclusions)
9. [Documentation](#documentation)
 
### Objectives 
The primary objective of this lab was to create a program that will display box and use pin outputs to use this. The purpose of this exercise is to develop an understanding of how to use peripherals and write/implement code that will allow you to interface with a display. 

### Preliminary design
To even start this lab there are some values you need to know. Look at the pinmaps and for the LCD BoosterPack,and Msp430. Also you need to examine the subroutines provided to see how they work, such as draw pixel and draw box. All can be viewed in the attached prelab.
#### Pseudocode:
My pseudo-code that I planned to use for the key subroutines
#####DrawBox
store values

define start

loop

draw row

decrement	 

ret
#####PollPins
check pin

  active? if not jmp next pin

  delay

  delete old box (draw over)

  draw new box 10 left/right/etc

#####Bonus
check pin

  draw l1

  draw l2

  draw l3

  draw l4

  draw l5  

ret

drawl1

  set params

  draw line

  set params

  draw line

ret
### Code:

######DrawBox Subroutine: 
	drawBox:
		push	r6
		push	r7
		push	r8
		push	r12
		push	r13
		push	r14
		push	r15
		call	#setArea
		mov		#0xFFFF, r12	;0x0000 for black or 0xFFFFF for white; other colors in between
		call	#splitColor	;returns r12 with MSB of color, r13 the LSB
		mov		#10, r7
		mov		#10, r8
	paintBox:
		call	#writeData
		mov.b	r12, r6
		mov.b 	r13, r12
		call	#writeData
		mov.b	r6, r12		;r12 returned for color MSB 	preservation
		dec		r7
		jnz		paintBox	;paint all columns
		mov		#10, r7
		dec		r8
		jnz		paintBox	;paint each row, too
		pop		r15
		pop		r14
		pop		r13
		pop		r12
		pop		r8
		pop		r7
		pop		r6
		ret
######Delay Button Polling:
	;----------------------------------------------------------- 
	poll_s3:					;Move Down
		bit.b	#BIT1, &P2IN
		jnz		poll_s4		; button is active low
		;call	#clearScreen
		call	#clearBox
		call	#Delay160ms
		add		#YLINELEN,	r13			; changes y position
		call	#move_line
	;-------------------------------------------------------------------------------
######Bonus: Draws Name (only one letter depicted)
	;bonus:
		call 	#Delay160ms
		bit.b	#BIT3,	&P1IN
		jz		drawName
		jmp		bonus

	drawName:
		call	#drawW
		call	#drawA
		call	#drawG
		call	#drawS
		call	#drawEXC
		ret
	drawW:
		mov		#8,			R12
		mov		#60,		R13
		mov		#8,			R14
		mov		#116,		R15
		call	#drawLine

		mov		#24,		R12
		mov		#60,		R13
		mov		#24,		R14
		mov		#116,		R15
		call	#drawLine

		mov		#40,		R12
		mov		#60,		R13
		mov		#40,		R14
		mov		#116,		R15
		call	#drawLine

		mov		#8,			R12
		mov		#116,		R13
		mov		#40,		R14
		mov		#116,		R15
		call	#drawLine

		ret
### Debugging
I had some problems with my delay routine, my button polling, and my bonus.

#####Delay Routine: 
  Not running through: declare value outside of loop, use Push/Pop instead of NOPs, 
#####Button Polling:
  Not Building: Check spelling and proper Bytes/Pins
  Not Responding: Comment out P1, same bytes being used
  Not Moving Right: Go back through parameters
#####Bonus:
  Not Building: spelling held me up for about 45 minutes
  Lines Drawn incorrectly: 
### Testing results
So there I was pushing a bytes  on mosi line, flowchat said I shold have these commands. 

### Logic Analyzer
To communicate the chips sends individual bytes of info to the lcd. 
To unravel the story the bits are sending tells us first we need to hook up our pins to the logic analyzer as pictured below.

![Memory Dump](images/PinMap.jpg)
##### Figure 1:
We are looking for the pin map to send 11 bytes from CASET to RAMWR in communication as shown in the flowchart below
![Memory Dump](images/FlowChart.JPG)
##### Figure 2: An Overview chart for everything
We can see each 8 bit packet
![Memory Dump](images/Overview.jpg)
##### Figure 3: Logic Overview all 11 bits
The first values sent on the MOSI line are 00101010 (you can just trace the colored lines pinned to the rising edge on the clock cycle and see what value MOSI is at each). This is 2A in hex which corresponds to Column address set, just what we were looking for.
![Memory Dump](images/Byte1.jpg)
##### Figure 4:First Byte
This command defines a memory frame the MCU can access, they SC[15:0] is the next two bytes and EC[15:0] are the two after that

![Memory Dump](images/CAS.jpg)
##### Figure 5 Column graphic
XStart MSB:    00000000
![Memory Dump](images/Byte2.jpg)
##### Figure 5:Byte 2
XStart other half:	00001101
![Memory Dump](images/Byte3.jpg)
##### Figure 6:Byte 3
XEnd MSB: 	00000000
![Memory Dump](images/Byte4.jpg)
##### Figure 7:Byte 4
xEnd Other half:    00010010
![Memory Dump](images/Byte5.jpg)
##### Figure 8:Byte 5
Now we run into another command: 00101011 or 2B 
This is Page Address Set
![Memory Dump](images/Byte6.jpg)
##### Figure 9: Byte 6
This command defines a memory frame the MCU can access, they SP[15:0] is the next two bytes and EP[15:0] are the two after that
![Memory Dump](images/PAS.jpg)
##### Figure 10: Row graphic
YStart MSB: 00000000
![Memory Dump](images/Byte7.png)
##### Figure 11:Byte 7
YStart other half:  00000111
![Memory Dump](images/Byte8.png)
##### Figure 12:Byte 8
YEnd MSB:	00000000
![Memory Dump](images/Byte9.png)
##### Figure 13:Byte 9
YEnd other half:	00000111
![Memory Dump](images/Byte10.png)
##### Figure 14:Byte 10
The last is 00101100 or 2C which means memory write. storing and finishing the whole packet.
![Memory Dump](images/Byte11.png)
##### Figure 15:Byte 11

And that's it!
![Memory Dump](images/chart.jpg)
##### Figure 16: An Overview chart for everything
The majority of these bytes are as expected however the ones that were not a command and were not zero may be incorrect because I ran the chip in debug mode and pressed the button multiple times which can cause the registers to change values and thus send the chip info other than the specific offsets that would be expected at the start.
### Lab Questions
Here is my filled out chart.
![Memory Dump](images/question.jpg)
##### Figure 17: Lab Questions

### Observations and Conclusions
The objectives of the lab were to draw a moving box and then write your name, both of which I was able to accomplish so I would consider it to be a success. Along the way I also improved my coding ability and learned about controlling external chips with the MSP430 as well as using the logic analyzer. This represents a big step for us in applications of embedded systems. I will be glad to move on from assembly to C

### Documentation
Prelab

Jake Magness: Helped me find my speed and we discussed the delay subroutine
Bimba Orikogbo: Helped me figure out what values were needed in the registers and which hex values would help them. (Found on the Second chart)
Tylar Hansen: Explained that the commands change on the falling edge and helped me with the values so I could make my waveform

Lab

Anthony T. : Helped me make my draw box by recommending the modification of the clearScreen. Also suggested a clear program for when trying to make the box move. He also helped me set up the pins on my logic analyzer. 

Mario B. : Helped me Debug my movebox and recommended the push-pop method to burn cycles since my nop method messed with the flags. 

Chris D. : Helped me with the bonus functionality with drawing lines in the grid, he also helped me debug this function and we worked together to figure out the logic analyzer portion.
