# Lab 2 - Subroutines - "Cryptography"

## By C2C Bret Wagner

## Table of Contents 
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Preliminary Design](#preliminary-design)
3. [Code](#code)
4. [Debugging](#debugging)
5. [Testing methodology or results](#testing-methodology-or-results)
6. [Observations and Conclusions](#observations-and-conclusions)
7. [Documentation](#documentation)
 
### Objectives or Purpose 
The purpose of this lab is to practice writing subroutines and coding by creating a simple decoder. The decoder will decode by using the simple method of XORing the method and code and storing the result.

Required Functionality: Use the XOR command to decode a message with a single byte long key.

B Functionality: Decode a message as before with a multi-byte long key

A Functionality: Decode a message with only knowledge of the key-length
### Preliminary design
To attack this problem I will have to develop two subroutines, one that separates each byte of the encrypted message and each byte of the key and a second subroutine that actually uses the XOR command.
For A functionality I will need to create another subroutine that actually finds the key, before it can be used by the decoder. This will have to iterate key guesses until it finds one that yields Askey values for the whole encrypted message.


#####Flowchart

![Memory Dump](images/chart.JPG)
##### Figure 1: Flowchart

### Code:
My decryptMessage subroutine segments the encrypted message and they key into bytes that it sends to the decryptByte subroutine. It continues until the message is decoded (see the jne decryptMessage)

	;-------------------------------------------------------------------------------
	decryptMessage:
			mov.b	0(r6),		r7
            call    #decryptByte
            mov.b	r7,			0(r9)
            inc		r9
            inc		r6
         	call	#resetPointer
            cmp		r11, 		r6
            jne		decryptMessage
            ret
	;-------------------------------------------------------------------------------
This resetPointer subroutine keeps track of where the message is  so the right bytes of the key are used and the message ends on time.


	;-------------------------------------------------------------------------------
	resetPointer:
			inc		r4				;incraments key length
			inc		r10
			cmp		r10,		r5
			jne		decryptMessage
			mov.w	r11,		r4
			mov.w	#Zero,		r10
			ret
	;-------------------------------------------------------------------------------

The section below is my decryptByte subroutine its just a simple xor that returns to the other subroutine.

	decryptByte:							;Pretty simple 
			xor.b			0(r4),	r7
            ret


### Debugging
There were several main challenges in debugging. The first part was getting the values to store to the right place in memory. The second challenge was in deciding to decide between using jump commands and call/return commands. I also ended up having to re-work some of those issues when I tried to add B and then A functionality. 

### Testing methodology 
My testing methodology was relatively simplistic: debug until it builds, then run and modify 
First put the test case into ROM
![Memory Dump](images/2.JPG)
##### Figure 1: Test Values
Build and run the program then hit pause
![Memory Dump](images/3.JPG)
Look into register r9 for the memory location
##### Figure 2: Register Viewer
Find that value in the memory browser 0x0251 then look just above it for the decoded message.
![Memory Dump](images/4.JPG)
##### Figure 3: Memory Browser


### Observations and Conclusions
I would consider this lab a relative success with respect to functionality and understanding. While I wished I had gotten the A to work I am glad that I understand the logic and process that would be used to get there. My understanding of this type of cryptography definitely went from 0 to 60 on this assignment and it was nice to learn something that I could directly connect to the real world. It was also nice to get better understanding of the call and return functions and the relative advantage they have over jump functions they have in the stack. On a more grim note I learned that I am a messy and somewhat inadequate programmer, the logic always makes sense to me, but I have yet to be able get a program to work from start to finish without any help on debugging. 
### Documentation
Mario Bracomonte: Helped me dubugg my code to get it spitting out the correct values
Jason Pluger: Helped me conceptualize the flowchart and code flow
Captain Warner: Helped me understand how to implement A functionality, although I never got it working