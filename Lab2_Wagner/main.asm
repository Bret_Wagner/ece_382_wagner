;-------------------------------------------------------------------------------
; MSP430 Assembler Code Template for use with TI Code Composer Studio
;Bret Wagner
;T3
;Lab1
;Documenatation:
;-------------------------------------------------------------------------------
            .cdecls C,LIST,"msp430.h"       ; Include device header file
            
;-------------------------------------------------------------------------------
            .def    RESET                   ; Export program entry-point to
                                            ; make it known to linker.
;-------------------------------------------------------------------------------
            		.text                      ; Assemble into program memory.
												    ; references to current section.
;Declares encrypted messages and keys
;NewKey:			.byte
;EncryptedMessage:	.byte		0xef,0xc3,0xc2,0xcb,0xde,0xcd,0xd8,0xd9,0xc0,0xcd,0xd8,0xc5,0xc3,0xc2,0xdf,0x8d,0x8c,0x8c,0xf5,0xc3,0xd9,0x8c,0xc8,0xc9,0xcf,0xde,0xd5,0xdc,0xd8,0xc9,0xc8,0x8c,0xd8,0xc4,0xc9,0x8c,0xe9,0xef,0xe9,0x9f,0x94,0x9e,0x8c,0xc4,0xc5,0xc8,0xc8,0xc9,0xc2,0x8c,0xc1,0xc9,0xdf,0xdf,0xcd,0xcb,0xc9,0x8c,0xcd,0xc2,0xc8,0x8c,0xcd,0xcf,0xc4,0xc5,0xc9,0xda,0xc9,0xc8,0x8c,0xde,0xc9,0xdd,0xd9,0xc5,0xde,0xc9,0xc8,0x8c,0xca,0xd9,0xc2,0xcf,0xd8,0xc5,0xc3,0xc2,0xcd,0xc0,0xc5,0xd8,0xd5,0x8f
EncryptedMessage:	.byte		0xf8,0xb7,0x46,0x8c,0xb2,0x46,0xdf,0xac,0x42,0xcb,0xba,0x03,0xc7,0xba,0x5a,0x8c,0xb3,0x46,0xc2,0xb8,0x57,0xc4,0xff,0x4a,0xdf,0xff,0x12,0x9a,0xff,0x41,0xc5,0xab,0x50,0x82,0xff,0x03,0xe5,0xab,0x03,0xc3,0xb1,0x4f,0xd5,0xff,0x40,0xc3,0xb1,0x57,0xcd,0xb6,0x4d,0xdf,0xff,0x4f,0xc9,0xab,0x57,0xc9,0xad,0x50,0x80,0xff,0x53,0xc9,0xad,0x4a,0xc3,0xbb,0x50,0x80,0xff,0x42,0xc2,0xbb,0x03,0xdf,0xaf,0x42,0xcf,0xba,0x50;EncryptedMessage:	.byte		0x35,0xdf,0x00,0xca,0x5d,0x9e,0x3d,0xdb,0x12,0xca,0x5d,0x9e,0x32,0xc8,0x16,0xcc,0x12,0xd9,0x16,0x90,0x53,0xf8,0x01,0xd7,0x16,0xd0,0x17,0xd2,0x0a,0x90,0x53,0xf9,0x1c,0xd1,0x17,0x90,0x53,0xf9,0x1c,0xd1,0x17,0x9e
;Key:				.byte		0xac
Key:				.byte		0xac, 0xdf, 0x23


;MessageLength		.equ	42
					.data					;writes to RAM
DecryptedMessage:	.space	40				;allots space to RAM


				.text							;prevents from becoming RAM

;-------------------------------------------------------------------------------
RESET       mov.w   #__STACK_END,SP         ; Initialize stackpointer
StopWDT     mov.w   #WDTPW|WDTHOLD,&WDTCTL  ; Stop watchdog timer


;-------------------------------------------------------------------------------
; Main loop here
;-------------------------------------------------------------------------------

            ;
            ; load registers with necessary info for decryptMessage here
            ;
;Set up all of the bytes
KeyLength:			.equ 		3
Zero:				.equ		0
			mov.b	#KeyLength,				r5 ;moving declared values into registers so they can be manipulated without changing
			mov.b	#Zero,					r10
			mov.w	#Key,					r4
			mov.w	#Key,					r11
			mov.w	#EncryptedMessage,		r6
			mov.w	#DecryptedMessage,		r9
            call    #decryptMessage


forever:    jmp     forever

                                            ; Subroutines
;-------------------------------------------------------------------------------
;Subroutine Name: findKey
;Author:	Bret Wagner
;Function: Finds our key by xoring different bytes to see if there are askey values
;Inputs: encrypted message, keylength
;Outputs: newkey
;Registers destroyed: r8 r10
;-------------------------------------------------------------------------------
;Left this incomplete, all attempts failed
findkey:

			ret
;-------------------------------------------------------------------------------
;Subroutine Name: decryptMessage
;Author:	Bret Wagner
;Function: Decrypts a string of bytes and stores the result in memory.  Accepts
;           the address of the encrypted message, address of the key, and address
;           of the decrypted message (pass-by-reference).  Accepts the length of
;           the message by value.  Uses the decryptCharacter subroutine to decrypt
;           each byte of the message.  Stores theresults to the decrypted message
;           location.
;Inputs: message, key, message length, key length,
;Outputs: decrypted message
;Registers destroyed: r4,r5,r6,r9,r10
;-------------------------------------------------------------------------------
;Decides what bytes and key secton to send decryptByte, loops untill message is decoded
decryptMessage:
			mov.b	0(r6),		r7
            call    #decryptByte
            mov.b	r7,			0(r9)
            inc		r9
            inc		r6
         	call	#resetPointer
            cmp		r11, 		r6
            jne		decryptMessage
            ret
;Keeps track of location within encrypted message and the key
resetPointer:
			inc		r4				;incraments key length
			inc		r10
			cmp		r10,		r5
			jne		decryptMessage
			mov.w	r11,		r4
			mov.w	#Zero,		r10
			ret
;-------------------------------------------------------------------------------
;Subroutine Name: decryptByte
;Author:	Bret Wagner
;Function: Decrypts a byte of data by XORing it with a key byte.  Returns the
;           decrypted byte in the same register the encrypted byte was passed in.
;           Expects both the encrypted data and key to be passed by value.
;Inputs: encripted byte, key byte
;Outputs: decrypted byte
;Registers destroyed:r7
;-------------------------------------------------------------------------------

decryptByte:							;Pretty simple xor
			xor.b			0(r4),	r7
            ret

;-------------------------------------------------------------------------------
;           Stack Pointer definition
;-------------------------------------------------------------------------------
            .global __STACK_END
            .sect    .stack

            
;-------------------------------------------------------------------------------
; Interrupt Vectors
;-------------------------------------------------------------------------------
            .sect   ".reset"                ; MSP430 RESET Vector
            .short  RESET
            
