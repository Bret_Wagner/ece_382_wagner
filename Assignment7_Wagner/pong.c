/*--------------------------------------------------------------------
Name: Bret Wagner
Date: 10/10/16
Course: ECE 382
File: Assignment7_Wagner.asm
Event: Assignment 7

Purp: Plays Pong
Doc:

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/
#include "pong.h"

ball createball(int xPos, int yPos, int xVel, int yVel, unsigned char radius)
{
	vectorxy posi;
	posi.x = xPos;
	posi.y = xPos;

	vectorxy velo;
	velo.x = xVel;
	velo.y = yVel;

	ball dball;
	dball.position = posi;
	dball.velocity = velo;
	dball.radius = radius;
	return dball;
}

ball moveball(ball nextball)
{
unsigned char checkleft(ball newball);
unsigned char checkright(ball newball);
unsigned char checktop(ball newball);
unsigned char checkbottom(ball newball);

	if(checkleft(nextball))
	{
		nextball.velocity.x *=-1;
	}
	if(checkright(nextball))
	{
		nextball.velocity.x *=-1;
	}
	if(checktop(nextball))
	{
		nextball.velocity.y *=-1;
	}
	if(checkbottom(nextball))
	{
		nextball.velocity.y *=-1;
	}
	nextball.position.x += nextball.velocity.x;
	nextball.position.y += nextball.velocity.y;

	return nextball;
}

unsigned char checkleft(ball newball)
{
	if(newball.position.x < 5)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

unsigned char checkright(ball newball)
{
	if(newball.position.x > WIDTH-5)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

unsigned char checkbottom(ball newball)
{
	if(newball.position.y < 5)
	{
		return 1;
	}
	else
	{
		return 0;
	}

}

unsigned char checktop(ball newball)
{
	if(newball.position.y > HEIGHT-5)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

main();						//calls function
