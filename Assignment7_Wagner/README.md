To check my edge cases I set the width and height to 50 instead of 500

###ScreenShots:
Start

![Memory Dump](images/a.JPG)

4 steps

![Memory Dump](images/b.JPG)

6 steps, hits edge

![Memory Dump](images/c.JPG)

Hits! 2 edge cases trigger

![Memory Dump](images/d.JPG)

11 steps

![Memory Dump](images/e.JPG)

12 step should trigger other 2 edge cases

![Memory Dump](images/f.JPG)

They Trigger! It works!

![Memory Dump](images/g.JPG)
###Questions
####How did you verify your code functions correctly?
I opened the debugger and looked at my variables. I stepped through and watched for the edge cases to trigger and then for the velocities to change. I used corners so I only had to do 2 instead of 4. I also took down the size of the area so it was faster.
####How could you make the "collision detection" helper functions only visible to your implementation file (i.e. your main.c could not call those functions directly)?
You can't put it in your header file. I think the way I did it makes it "invisible"
