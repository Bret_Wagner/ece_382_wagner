/*--------------------------------------------------------------------
Name: Bret Wagner
Date: 10/10/16
Course: ECE 382
File: Assignment7_Wagner.asm
Event: Assignment 7

Purp: Plays Pong
Doc:

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/
#include <msp430.h>

#include "pong.h"
#define a 0x38//gloabal constant, notice set up without ";"

int main(void) {
    WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer
    int cycles = 1000;
    ball gameball = createball(xStart,yStart,xvStart,yvStart,radStart);
    while(cycles>1)
    {
    	gameball = moveball(gameball);
    	cycles--;
    }
}
main();						//calls function
