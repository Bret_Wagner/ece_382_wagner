
/*--------------------------------------------------------------------
Name: C2C Bret Wagner
Date: 20 July 2016
Course: ECE 382
File: pong.h
Event: Assignment 7 - Pong

Purp: Implements a subset of the pong game

Doc:    I got this header from Captain Falkinberg and modified it slightly

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/

#ifndef _PONG_H
#define _PONG_H

#define WIDTH 50
#define HEIGHT 50
#define xStart 25
#define yStart	25
#define xvStart 5
#define yvStart 5
#define radStart 5
typedef struct {
    int x;
    int y;
} vectorxy;

typedef struct {
    vectorxy position;
    vectorxy velocity;
    unsigned char radius;
} ball;

ball createball(int xPos, int yPos, int xVel, int yVel, unsigned char radius);

ball moveball(ball nextball);


#endif
