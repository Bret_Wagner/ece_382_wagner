;-------------------------------------------------------------------------------
; MSP430 Assembler Code Template for use with TI Code Composer Studio
;Bret Wagner
;T3
;Lab1
;Documenatation: Worked with Mario Bracamonte
;-------------------------------------------------------------------------------
            .cdecls C,LIST,"msp430.h"       ; Include device header file
            
;-------------------------------------------------------------------------------
            .def    RESET                   ; Export program entry-point to
                                            ; make it known to linker.
;-------------------------------------------------------------------------------
        	    .text                       ; Assemble into program memory.
;Taking the test condition and storing in RAM
myProgram:      .byte	0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0xDD, 0x44, 0x08, 0x22, 0x09, 0x44, 0xFF, 0x22, 0xFD, 0x55

                .data
                .sect	".reset"
                .sect 	.stack

myResults:      .space      20                          ; reserving 20 bytes of space
				.text
;-------------------------------------------------------------------------------
RESET       mov.w   #__STACK_END,SP         ; Initialize stackpointer
StopWDT     mov.w   #WDTPW|WDTHOLD,&WDTCTL  ; Stop watchdog timer


;-------------------------------------------------------------------------------
; Main loop here
;-------------------------------------------------------------------------------
;Sets all variables so there are no "magic numbers"
ADD_OP:         .equ        0x11
SUB_OP:         .equ        0x22
MUL_OP:         .equ        0x33
CLR_OP:         .equ        0x44
END_OP:         .equ        0x55
min:			.equ		0x00
max:			.equ		0xff
;grabbs first instruction from program
				mov.w		#myResults,	r4
				mov.w		#myProgram,	r5
;Takes the First operand
inputnum:
				mov.b		0(r5),		r6
				inc.w		r5
;Chooses the Operator which applies
;If the input was not a known operator it defaults to clear
inputopp:
				mov.b		0(r5),		r7
				inc.w		r5
				cmp			#ADD_OP,	r7
				jeq			add
				cmp			#SUB_OP,	r7
				jeq			sub
				cmp			#CLR_OP,	r7
				jeq			clr
				cmp			#END_OP,	r7
				jeq			end
				jmp			clr
;Adds the two numbers
add:
				add.b		0(r5),		r6
				jc			big
				jmp			store
;Subtracts the two numbers
sub:
				mov.b		0(r5),		r9
				sub.w		r9,			r6
				jn			small
				jmp			store

;Clears the current operand to 0x00 and starts over
clr:
				mov.b		#min,		r6
				mov.b		r6,			0(r4)
				inc.w		r4
				jmp			inputnum
;Checks if the number is above max value ff, replaces with ff if true
big:
				mov.b		#max,		0(r4)
				inc.w		r4
				inc.w		r5
				jmp			inputopp
;Checks if the number is below max value ff, replaces with 0 if true
small:
				mov.b		#min,		r6
;Stores the value
store:
				mov.b		r6,			0(r4)
				inc.w		r4
				inc.w		r5
				jmp			inputopp
end:				jmp			end
                                            

;-------------------------------------------------------------------------------
; Stack Pointer definition
;-------------------------------------------------------------------------------
            .global __STACK_END
            .sect   .stack
            
;-------------------------------------------------------------------------------
; Interrupt Vectors
;-------------------------------------------------------------------------------
            .sect   ".reset"                ; MSP430 RESET Vector
            .short  RESET
            
