# Lab #1 - Assembly Language - "A Simple Calculator"
## By  C2C Bret Wagner

## Table of Contents 
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Preliminary Design](#preliminary-design)
3. [Software flow chart](#software-flow-chart)
4. [Well-formatted code](#code)
5. [Debugging](#debugging)
6. [Testing methodology or results](#testing-methodology-or-results)
8. [Observations and Conclusions](#observations-and-conclusions)
9. [Documentation](#documentation)
 
### Objectives 
The objective of this assignment is to use assembly language to create a simple calculator that meets the functionality described below. This activity will help solidify understanding of jumps, flags, addressing modes, and other skills through application and practice. 

####Required Functionality
Given positive integer inputs from 0-255 in ROM, store results of calculation using the operations below in RAM starting at 0x0200

#####Requred Operations
ADD_OP
Coded by the value 0x11. Adds two numbers and stores the result.

SUB_OP
Coded by the value 0x22. 
Subtracts two numbers and stores the result.


CLR_OP
Coded by the value 0x44, 
Clears the result by storing 00 to memory. It then uses the second operand as the initial value for the next operation.

END_OP
Coded by the value 0x55.
Terminates execution of the calculator

##### B-Functionality
Store values above 0xff as oxff and values below 0x00 as Ox00

### Preliminary design
To star with the design I thought about an iterative process, grab the values and the operation, preform the operation, store the result, then grab the next value and operation and repeat till the end. I knew this would involve a step to choose the operation, coding for the operations and another section to make sure that the right value was stored (not above 255 or below 0).

### Software Flow Chart
#### Initial
![Memory Dump](images/InitialFlowChart.PNG)
######## First flow chart
#### Final

![Memory Dump](images/FinalFlowChart.PNG)
######## Final flow chart
My final flow chart reflects the way that my program actually took shape with little exception. I took out the multiply command because time constraints prevented me from achieving its functionality. I modified my instructions to be more friendly to a non-engineer and added decision points. I also realized that because my program didn't include multiplication there was no reason to check high and low on the same output because low (negative) outputs could only be achieved through subtraction. Likewise high (above 255) outputs could only be achieved through addition, so I was able to modify those for simplification. I also had to take my clear command out of the normal progression because it does not store a result and just take the next operand like the other instructions, so I diverted its path from the regular store command and gave it its own.
### Code:
#####Important Code:
The section below is important because it shows how a test case is stored in ROM

	;-------------------------------------------------------------------------------
					.text
	myProgram:      .byte	0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0xDD, 0x44, 0x08, 0x22, 0x09, 0x44, 0xFF, 0x22, 0xFD, 0x55
	;-------------------------------------------------------------------------------
This section is neat because it labels variables


	;-------------------------------------------------------------------------------
	.text
	ADD_OP:         .equ        0x11
	SUB_OP:         .equ        0x22
	MUL_OP:         .equ        0x33
	CLR_OP:         .equ        0x44
	END_OP:         .equ        0x55
	min:			.equ		0x00
	max:			.equ		0xff
	;-------------------------------------------------------------------------------

The section below was the main decision point that chooses which compares values to determine which operation is executed

	inputopp:
				mov.b		0(r5),		r7
				inc.w		r5
				cmp			#ADD_OP,	r7
				jeq			add
				cmp			#SUB_OP,	r7
				jeq			sub
				cmp			#CLR_OP,	r7
				jeq			clr
				cmp			#END_OP,	r7
				jeq			end
				jmp			clr
### Debugging
I encountered three types of errors in my construction of this program
##### Syntax
Some missed commas, semicolons, and octothorpe's caused me a little trouble compiling and running. 
##### Addressing
I mixed up my addressing modes on some of my instructions which caused errors in the way things were stored/if they worked at all. It was a little confusing to keep everything strait and reminds me to start with a clear plan on the types of addressing to use from the very beginning.
##### Logic
The most difficult bugs to get out were those caused by flaws in my logic. The main instance I can think of is when I was incrementing to grab the next instruction. I have this store command which I thought would be convenient for every operation. This was all true except for the clear command which works differently. I tried to decrement in my clear function so when I incremented in store it would even out, but that caused problems in the way I was storing my results. My attempts to use less lines of code ended up causing the most trouble for me.
### Testing methodology or results
Testing the project was fairly simple given the test conditions I would load the string into ROM and run it step by step to see if everything worked as expected. I made sure to use different test cases to make sure each operator was functional.
Below is an example of how it worked using this test case.

First put the test case into ROM
![Memory Dump](images/test1.JPG)
Build and run through debugger, set up memory in 8-Bit Hex Ti style open up registers and look at r4
![Memory Dump](images/test2.JPG)
Go step by step until r4 changes and then go to the memory location indicated in this case 0x03EC
Also set a stop at line 50 
![Memory Dump](images/test3.JPG)
Click the green run button which will go through a loop once, watch your registers for the flags you set and see how the memory starts to fill. Each click is another operation
![Memory Dump](images/test4.JPG)
Another operation goes by
![Memory Dump](images/test5.JPG)
All test cases filled, CPU trap engaged
![Memory Dump](images/test6.JPG)
This is how I tested, if anything went wrong, I would restart the debugger and click step by step and watch the registers and memory locations to see what went wrong.

### Observations and Conclusions
Using the techniques provided in the last several lesson I was able to successfully meet all the requirements. I felt my programming skills improved as well as my understanding of addressing and memory. I was a little disappointed in myself for not finishing A-functionality, which is more a a lesson in time management than anything else. It would be cool to see how the calculator could be expanded upon by adding other functions or possibly changing the order of operations. 

### Documentation
Mario Bracomonte: Helped me with my debugging, and helped fix my source-tree. We also talked about the pre-lab and lab notebook together. Great guy.