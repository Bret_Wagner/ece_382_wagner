;-------------------------------------------------------------------------------
; MSP430 Assembler Code Template for use with TI Code Composer Studio
;Bret Wagner
;T3
;Assignment 3 code
;
;-------------------------------------------------------------------------------
            .cdecls C,LIST,"msp430.h"       ; Include device header file
            
;-------------------------------------------------------------------------------
            .def    RESET                   ; Export program entry-point to
                                            ; make it known to linker.
;-------------------------------------------------------------------------------
            .text                           ; Assemble into program memory.
            .retain                         ; Override ELF conditional linking
                                            ; and retain current section.
            .retainrefs                     ; And retain any sections that have
                                            ; references to current section.

;-------------------------------------------------------------------------------
RESET       mov.w   #__STACK_END,SP         ; Initialize stackpointer
StopWDT     mov.w   #WDTPW|WDTHOLD,&WDTCTL  ; Stop watchdog timer


;-------------------------------------------------------------------------------
; Main loop here
;-------------------------------------------------------------------------------
	mov.w 	#0x0216, 	r5	;Sets Values for compare and loops
	mov.w	#1234,		r6
	mov.w	#0x0206,	r10
	mov.w	#0x1000,		r7
	mov.w	#20,			r8
	mov.w	#0,			r9
	clr		0(r10)
	mov.w	#0xedcb,		r11
	mov.w	#0x0212,		r14
	add.w	0(r5),		r11
	jc		greater1
	jmp		less1

greater1:					;Condition if value is greater than 0x1234
	add.w	r8,	0(r10)
	dec.w	r8
	jnz		greater1
	mov.w	r8, 0(r5)
	jmp 	loop

less1:						;Condition if value is less than 0x1234 determines jump to greater or less
	mov.w	#0xefff,	r11
	add.w	0(r5),		r11
	jc		greater2
	jmp		less2

greater2:					;Condition if value is greater than 0x1000
	clrc					;clears c
	mov.w	#0x0202,	r12
	clr		0(r12)
	add.w   #0xEEC0, 	0(r5)
	dadc.b	0(r12)
	jmp		loop
less2:						;Condition is value is less than 0x1000 determines jump to even or odd
	mov.w	#0x0212,	r11
	mov.w	0(r5),		r12
	and.w	#0x0001,	r12
	cmp		#0x0000,	r12
	jz		even
	jmp		odd
even:						;Divides the even value
	rra.w	0(r5)
	mov.w	0(r5),	0(r14)
	jmp 	loop
odd:						;Multiplies the odd value
	rla.w	0(r5)
	mov.w	0(r5),	0(r14)
	jmp 	loop

loop:
	jnz 	loop			;endless loop

                                            

;-------------------------------------------------------------------------------
; Stack Pointer definition
;-------------------------------------------------------------------------------
            .global __STACK_END
            .sect   .stack
            
;-------------------------------------------------------------------------------
; Interrupt Vectors
;-------------------------------------------------------------------------------
            .sect   ".reset"                ; MSP430 RESET Vector
            .short  RESET
            
