#include <msp430.h>
#include "moves.h"


int8	newIrPacket = FALSE;
int16	packetData[32];
int8	packetIndex = 0;
int32   irPacket;


void initMSP430() {

	WDTCTL=WDTPW+WDTHOLD; 		// Full WD

	BCSCTL1 = CALBC1_8MHZ;
	DCOCTL = CALDCO_8MHZ;

	P2SEL &= ~BIT6;									// Set up P2.6 as GPIO not XIN
	P2SEL2 &= ~BIT6;									// This action takes
	P2DIR &= ~BIT6;									// three lines of code.
										// Set up P2.6 as GPIO not XIN
										// Once again, this take three lines
										// to properly do

						// check the header out.  P2IES changed.
	P1DIR |= BIT0;
	P1DIR |= BIT6;
	P1OUT &= ~BIT6;
	P1OUT &= ~BIT0;								// And turn the LEDs off

										// create a 16ms roll-over period
	TA0CCR0 = 0x3E80;
    TA0CTL &= ~TAIFG;
	TA0CTL |=	(ID_3|TASSEL_2|MC_1|TAIE);								// clear flag before enabling interrupts = good practice

	P2IFG &= ~BIT6;						// Clear any interrupt flag on P2.3
	P2IE  |= BIT6;						// Enable P2.3 interrupt

	HIGH_2_LOW;										// Use 1:8 prescalar off SMCLK and enable interrupts

	__enable_interrupt();
}


#pragma vector = PORT2_VECTOR			// This is from the MSP430G2553.h file

__interrupt void pinChange (void) {

	int8	pin;
	int16	pulseDuration;			// The timer is 16-bits

	if (IR_PIN)		pin=1;	else pin=0;

	switch (pin) {					// read the current pin level
		case 0:						// !!!!!!!!!NEGATIVE EDGE!!!!!!!!!!
			pulseDuration = TA0R;	//**Note** If you don't specify TA1 or TA0 then TAR defaults to TA0R
			if((pulseDuration <= maxLogic0Pulse) && (pulseDuration >= minLogic0Pulse)){
				irPacket = irPacket << 1;
				irPacket |= 0;
			}

			if((pulseDuration <= maxLogic1Pulse) && (pulseDuration >= minLogic1Pulse)){
							irPacket = irPacket << 1;
							irPacket |= 1;
			}

			TA0CTL = 0;
			LOW_2_HIGH; 				// Set up pin interrupt on positive edge
			break;

		case 1:							// !!!!!!!!POSITIVE EDGE!!!!!!!!!!!
			TA0R = 0x0000;						// time measurements are based at time 0
			TA0CTL |=	(ID_3|TASSEL_2|MC_1|TAIE);
			HIGH_2_LOW; 						// Set up pin interrupt on falling edge
			break;
	} // end switch

	P2IFG &= ~BIT6;			// Clear the interrupt flag to prevent immediate ISR re-entry

} // end pinChange ISR

// -----------------------------------------------------------------------
//			0 half-bit	1 half-bit		TIMER A COUNTS		TIMER A COUNTS
//	Logic 0	xxx
//	Logic 1
//	StarT
//	End
//
// -----------------------------------------------------------------------
#pragma vector = TIMER0_A1_VECTOR			// This is from the MSP430G2553.h file
__interrupt void timerOverflow (void) {

	TA0CTL &= ~TAIFG;
}
//All old above





//All new below
void control(){			//control polls for the input from the irSensor,
	while(1){
		if(irPacket== UP)
		{
			fwd();
			irPacket=0;
		}
		if(irPacket== DOWN)
		{
			back();
			irPacket=0;
		}
		if(irPacket== LEFT)
		{
			left();
			irPacket=0;
		}
		if(irPacket== RIGHT)
		{
			right();
			irPacket=0;
		}
		if(irPacket== VOL_UP)
		{
			halfleft();
			irPacket=0;
		}
		if(irPacket== VOL_DW)
		{
			halfright();
			irPacket=0;
		}
	}
}

void fr() // moves the right wheel foreword
{
    P2DIR |= BIT2;
    P2SEL |= BIT2;
    P2OUT &= ~BIT2;

    P2DIR |= BIT1;
    P2SEL &= ~BIT1;
    P2OUT |= BIT1;
}
void fl() // moves the left wheel foreword
{
    P2DIR |= BIT4;
    P2SEL |= BIT4;
    P2OUT &= ~BIT4;

    P2DIR |= BIT5;
    P2SEL &= ~BIT5;
    P2OUT |= BIT5;
}
void bl() //left wheel backword
{
    P2DIR |= BIT5;
    P2SEL |= BIT5;
    P2OUT &= ~BIT5;

    P2DIR |= BIT4;
    P2SEL &= ~BIT4;
    P2OUT |= BIT4;
}
void br() //right wheel backword
{
    P2DIR |= BIT1;
    P2SEL |= BIT1;
    P2OUT &= ~BIT1;

    P2DIR |= BIT2;
    P2SEL &= ~BIT2;
    P2OUT |= BIT2;
}
void fwd()
{
	fr();
	fl();

	TA1CTL &= ~MC_1; //sets ctl must be done before setting power
    TA1CCR1 = Fullp;
    TA1CCR2 = Fullp;
	TA1CTL |= MC_1;  //clears crl must be done after setting power

    __delay_cycles(Long);
	TA1CTL &= ~MC_1;
    TA1CCR1 = Stop;
    TA1CCR2 = Stop;
	TA1CTL |= MC_1;
}
void back()
{
	br();
	bl();

	TA1CTL &= ~MC_1;
    TA1CCR1 = Fullp;
    TA1CCR2 = Fullp;
	TA1CTL |= MC_1;

    __delay_cycles(Long);
	TA1CTL &= ~MC_1;
    TA1CCR1 = Stop;
    TA1CCR2 = Stop;
	TA1CTL |= MC_1;
}
void left()
{

	br();
	fl();
	TA1CTL &= ~MC_1;
    TA1CCR1 = Fullp;
    TA1CCR2 = Fullp;
	TA1CTL |= MC_1;

    __delay_cycles(Medium);
	TA1CTL &= ~MC_1;
    TA1CCR1 = Stop;
    TA1CCR2 = Stop;
	TA1CTL |= MC_1;
}
void right()
{
	fr();
	bl();
	TA1CTL &= ~MC_1;
    TA1CCR1 = Fullp;
    TA1CCR2 = Fullp;
	TA1CTL |= MC_1;

    __delay_cycles(Medium);
	TA1CTL &= ~MC_1;
    TA1CCR1 = Stop;
    TA1CCR2 = Stop;
	TA1CTL |= MC_1;
}
void halfleft()
{
	fr();
	bl();
	TA1CTL &= ~MC_1;
    TA1CCR1 = Fullp;
    TA1CCR2 = Fullp;
	TA1CTL |= MC_1;

    __delay_cycles(Short);
	TA1CTL &= ~MC_1;
    TA1CCR1 = Stop;
    TA1CCR2 = Stop;
	TA1CTL |= MC_1;
}
void halfright()
{
	br();
	fl();

	TA1CTL &= ~MC_1;
    TA1CCR1 = Fullp;
    TA1CCR2 = Fullp;
	TA1CTL |= MC_1;

    __delay_cycles(Short);
	TA1CTL &= ~MC_1;
    TA1CCR1 = Stop;
    TA1CCR2 = Stop;
	TA1CTL |= MC_1;
}
