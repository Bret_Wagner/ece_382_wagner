#include <msp430.h>
#include "moves.h"
void main(void)
{
    WDTCTL = WDTPW|WDTHOLD;                 // stop the watchdog timer

        TA1CTL |= TASSEL_2|MC_1|ID_0;           // configure for SMCLK

        TA1CCR0 = 1000;                // set signal period to 1000 clock cycles (~1 millisecond)
        TA1CCR1 = 200;
        TA1CCR2 = 200;  // set duty cycle to 250/1000 (25%)

        TA1CCTL0 |= CCIE;                // enable CC interrupts
        TA1CCTL1 |= OUTMOD_7;        // set TACCTL1 to Set / Reset mode//enable CC interrupts
        TA1CCTL1 &= ~CCIFG;                //clear capture compare interrupt flag
        TA1CCTL2 |= OUTMOD_7;        // set TACCTL1 to Set / Reset mode//enable CC interrupts
        TA1CCTL2 &= ~CCIFG;                //clear capture compare interrupt flag
        _enable_interrupt();
        initMSP430();
        control(); //enables remote control, a lot of the code around here isn't imporant
}

#pragma vector = TIMER1_A0_VECTOR            // This is from the MSP430G2553.h file
__interrupt void captureCompareInt (void) {
    P1OUT |= BIT0;                        //Turn on LED
    // Disable Timer A Interrupt
    TA1CCTL1 &= ~CCIFG;                //clear capture compare interrupt flag
//    TACTL &= ~TAIFG;
}

#pragma vector = TIMER1_A1_VECTOR            // This is from the MSP430G2553.h file
__interrupt void captureCompareInt2 (void) {
    P1OUT &= ~BIT0;                        //Turn off LED
    // Disable Timer A Interrupt
    TA1CCTL1 &= ~CCIFG;                //clear capture compare interrupt flag
//    TACTL &= ~TAIFG;
}
