# Lab 6 - PWM - "Robot Motion"

## By C2C Bret Wagner

## Table of Contents 
1. [Objectives](#objectives)
2. [Purpose](#purpose)
3. [Prelab](#prelab)
4. [Debugging](#debugging)
5. [Code](#code)
5. [Testing methodology or results](#testing-methodology-or-results)
6. [Observations and Conclusions](#observations-and-conclusions)
7. [Documentation](#documentation)
 
### Objectives
Achieve functionality.
##### Required Functionality
Demonstrate movement forward, backward, a small (< 45 degree) turn left and right, and a large (> 45 dgree) turn left and right.  The robot should perform these movements sequentially, completely disconnected from a computer (no USB cord).
##### A Functionality
Control your robot with your remote!  You should use at least four different buttons on the remote: one each to indicate motion directions of forward, backward, left, and right. 
### Purpose
The purpose of this lab is to get out robot moving under its own power so we are able to progress further, by later adding sensors and other systems so it can navigate a maze. This is a also an educational experience where we get to tie some of the things we already know in with new hardware.

### Prelab
---	
##### Hardware Considerations
Which pins will output which signals I need? 

	-Pin 2.1 for TA1.1
	-Pin 2.2 for TA1.0

	-Pin 2.4 for TA1.1
	-Pin 2.5 for TA1.0
Which side of the motor will you attach these signals to? 

	I will be using the GPIO Global Pin In/Out so I can change the signal on the input or output
	I'll connect the Black to ground and the other to the pin, by switching inputs can it change.
	
	
How will you use these signals to achieve forward / back / left / right movement?

	Foreword: Alternate Left/Right positive signal
	Back: Alternate left/right negative signal
	Left: Right signal (1/2 #cycles for 45%)
	Right: left signal (1/2 #cycles for 45%)

##### PMW Subsytem
What are the registers I'll need to use?
 
	-Capture and Control registers
	-TACCTL, 
	-TACCRO, 
Which bits in those registers are important? 

	-CAP
	-CCI
	-COV
        P2DIR |= BIT1;                // TA1CCR1 on P2.1
        P2SEL |= BIT1;                // TA1CCR1 on P2.1
        P2OUT &= ~BIT1;

        TA1CTL |= TASSEL_2|MC_1|ID_0;           // configure for SMCLK
        P1DIR |= BIT0;            //use LED to indicate duty cycle has toggled
        P1REN |= BIT3;
        P1OUT |= BIT3;

        TA1CCR0 = 1000;                // set signal period to 1000 clock cycles (~1 millisecond)
        TA1CCR1 = 250;                // set duty cycle to 250/1000 (25%)
        TA1CCTL0 |= CCIE;                // enable CC interrupts
        TA1CCTL1 |= OUTMOD_7|CCIE;        // set TACCTL1 to Set / Reset mode//enable CC interrupts
        TA1CCTL1 &= ~CCIFG;                //clear capture compare interrupt flag
        _enable_interrupt();
Some of thee variables will be changed but this is what I am using as a guide for which bits are important and the sequence of how to set them up.
Its the same principal except instead of using an LED we are using a
##### Hardware
What other hardware is required?

	-Will need capacitors (100uf, .1uf, 10pf), wires, LDV33 regulator, robot, battery pack, MSP430, launchpad, motor driving chip, IR sensor (for remote). 

How will I wire it?

	- large capacitor (~100uF) across the 12V rail
 	 - To supplement current when motor draw spikes
	- smaller capacitor (~0.1uF) across the 5V rail
 	 - To smooth high frequency noise
	- small capactior (10pF) between the RST pin and ground
 	 - To smooth noise to the RST pin
	-Pins for programming
		- Connect TEST to Pin 17 of your MSP430
		- Connect RST to Pin 16 of your MSP430
		- Connect VCC to Pin 1 of your MSP430
##### Schematic
![Memory Dump](images/robot_top.jpg)
##### Figure 1: Given Example
I included this in the lab because it gives important visuals of each of the various components. 
**Red Circle** - decoupling capacitor placed across the 5V rail to prevent current fluctuation that may cause your MSP430 to reset.

**Light Blue Circle** - regulator that converts 5V to 3.3V to power your MSP430.  Hooking 5V directly up to your MSP430 would burn it!  See datasheet for more info.

**Pink Circle** - An MSP430!  For this lab, it might be beneficial to place your MSP430 chip in the breadboard and use the Launchpad as an in-circuit programmer (see tutorial).

**Purple Circle** - Wiring the programming pins form the Launchpad board to the appropriate pins on the MSP430 (see tutorial).  Note how there is no chip in the Launchpad - I'm placing it in the port on the robot purely as a holder.

**Orange Circle** - The motor driver chip!  This allows your MSP430 to control the motors without burning up due to the current requirements.  See datasheet for more info.
##### Plan
-Find the motor stall current for my motor

-Connect everything up

-Develop motor control using PM to trigger interrupts

-Program each individual motion

-program the motions in sequence

##### Figure 2: My Robot
![Memory Dump](images/myRobot.jpg)
The main differences in my robot are that the msp430 remains on the launchpad and that I have an IR sensor for A functionality as well.
##### Figure 3: Digital Schematic
![Memory Dump](images/schematic.jpg)
#####Pseudo-code
	Set up
		Set up Timer
			TA1CTL |= TASL2|MC1|ID0
		Set CCR0 with PWM value
			CCR0  = 1000
		Set CCR1 for left motor
			CCR1 = 800  (80% max)
		Set CCR2 for right motor
			CCR2 = 800
	Control loop
		Wait for Remote input
			check IR bar
		On command (4 of these)
			Enable PWM pin (power to left or right
			Enable GPIO pin (FWD/Back)
			Run time lenght
		FWD
			PWN both
			GPIO In (both)
		Back
			PWN both
			GPIO Out (both)
		Left
			PWN  left
			GPIO Out left
			GPIO In right
		Right	
			PWN  right
			GPIO Out right
			GPIO In left


### Debugging
I had a lot of difficulty dealing with the MSP430 on the hardware side. Trying to use the MSP430 externally from the launchpad did not want to work. After playing around with the multimeter, wiring, re-wiring and swapping components with others who had verified working parts I finally decided just to keep my MSP430 on the launchpad and it worked almost immediately. These problems made the whole wiring process take around 4 hours.

I had some software issues when it came to installing and using the fritzing software, I ended up having my schematic all wired before I was able to draw the digital schematic. I will however have it complete later so I can add onto it for labs 7 and 8

### Code
I was actually surprised by how little code was required for this project. I will have to expand upon what I have for labs 7 and 8, but it seems like the bulk of the code was put in for A functionality. Here are 3 important pieces that were added.

1. Remote Polling

	This section of code is what allows you to control the robot via the remote. It polls the ir sensor to see if the signal matches the bits of a specific button press and then calls a corresponding function.

		 void control(){			
			while(1){
				if(irPacket== UP)
				{
					fwd();
					irPacket=0;
				}
				if(irPacket== DOWN)
				{
					back();
					irPacket=0;
				}
				if(irPacket== LEFT)
				{
					left();
					irPacket=0;
				}
				if(irPacket== RIGHT)
				}
			}
		}

2. Wheel functions. I had four of these functions each which assigns a wheel to move a direction. I did this by assigning one bit to GPIO and one bit with the PMW signal.

		void fr() // moves the right wheel foreword
		{
		    P2DIR |= BIT2;
		    P2SEL |= BIT2;
		    P2OUT &= ~BIT2;
		
		    P2DIR |= BIT1;
		    P2SEL &= ~BIT1;
		    P2OUT |= BIT1;
3. Move functions. I had six of these functions to execute the actual movements. They call one or more wheel movement functions from above and run for a set period of time.
void fwd()
		{
			fr();
			fl();
		
			TA1CTL &= ~MC_1; //sets ctl must be done before setting power
		    TA1CCR1 = Fullp;
		    TA1CCR2 = Fullp;
			TA1CTL |= MC_1;  //clears crl must be done after setting power
		
		    __delay_cycles(Long); //determines run time
			TA1CTL &= ~MC_1;
		    TA1CCR1 = Stop;
		    TA1CCR2 = Stop;
			TA1CTL |= MC_1;
		}
### Testing methodology and results
Preforming tests and working the data I got from them back into my code was a big part of my process. When I first preformed my first test, the foreword function my robot moved backward, so I had to make an adjustment. When I first connected my motors I wasn't sure if 800 or 200 would be max power for me, so I started at 500 and worked up and then down to make the right determination. When it came to actually demonstrating the success, I just pressed the buttons and it worked.


###Conclusion
This lab was difficult but rewarding. It was really nice to get to use several different pieces of hardware and watch it all come together. As soon as I got a wheel spinning and the remote to respond the rest of the lab was actually pretty fun. I also feel like I learned a lot about the motor driver and voltage regulator and found them to be a lot more simple than the LCD Screen and the MSP430 itself. Additionally I was able to learn to use Fritzing which seems like a valuable tool. Overall I achieved A functionality so was obviously successful and hopefully will do just as well going into lab 7 and 8.

### Documentation
Mario Bracomonte and I were partners on the previous lab, therefore a lot of our code is the same because it was re-used from the last lab for the remote control part. 

Chris Dukharm: Helped me a bunch with wiring my robot and recognizing some errors within

Mario Bracomonte: Helped me with issues with motor power and helped 	`me debug my code to get functions to work with the remote.