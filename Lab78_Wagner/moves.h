
#ifndef MOVES_H_
#define MOVES_H_
//from lab 5


//-----------------------------------------------------------------
// Page 76 : MSP430 Optimizing C/C++ Compiler v 4.3 User's Guide
//-----------------------------------------------------------------
typedef		unsigned char		int8;
typedef		unsigned short		int16;
typedef		unsigned long		int32;
typedef		unsigned long long	int64;

#define		TRUE				1
#define		FALSE				0

//-----------------------------------------------------------------
// Function prototypes found in lab5.c
//-----------------------------------------------------------------
void initMSP430();
__interrupt void pinChange (void);
__interrupt void timerOverflow (void);


//-----------------------------------------------------------------
// Each PxIES bit selects the interrupt edge for the corresponding I/O pin.
//	Bit = 0: The PxIFGx flag is set with a low-to-high transition
//	Bit = 1: The PxIFGx flag is set with a high-to-low transition
//-----------------------------------------------------------------

#define		IR_PIN			(P2IN & BIT6)
#define		HIGH_2_LOW		P2IES |= BIT6
#define		LOW_2_HIGH		P2IES &= ~BIT6


#define		averageLogic0Pulse	0x020d
#define		averageLogic1Pulse	0x0645
#define		averageStartPulse	0x1100
#define		minLogic0Pulse		averageLogic0Pulse - 100
#define		maxLogic0Pulse		averageLogic0Pulse + 100
#define		minLogic1Pulse		averageLogic1Pulse - 100
#define		maxLogic1Pulse		averageLogic1Pulse + 100
#define		minStartPulse		averageStartPulse - 100
#define		maxStartPulse		averageStartPulse + 100

#define		PWR		0x02FD48B7
#define		ONE		0x02FD807F
#define		TWO		0x02FD40BF
#define		THR		0x02FDC03F

#define		VOL_UP	0x02FD58A7
#define		VOL_DW	0x02FD7887
#define		CH_UP	0x02FDD827
#define		CH_DW	0x02FDF807
#define 	UP		0x02FD9867
#define		DOWN	0x02FDB847
#define		ENTER	0x02FD50AF
#define		LEFT	0x02FD629D
#define		RIGHT	0x02FDE21D
#define		POWER	0x02FD48B7



//lab6
#define	    Fullp	1000
#define	    Halfp	200
#define	    Stop	1000
#define	    Long	2300000
#define		Medium	1580000
#define	    Short	2000000


void fl(); //foreword left
void fr(); //foreword right
void bl(); //backword left
void br(); //backword right
void fwd();
void back();
void left();
void right();
void halfleft();
void halfright();
void initMSP430();
void control();
int place;
// Lab 7
void echoklok();
__interrupt void echoChange (void);

#define sleft 1000;
#define sright 2000;
#define smiddle 1500;

#define		ECHO_PIN		(P1IFG & BIT4)
#define		HIGH_E_LOW		P1IES |= BIT4
#define		LOW_E_HIGH		P1IES &= ~BIT4

#define RISE	~P1IES&BIT4

void BL(); // Blink Left
void BR(); // Blink Right

void servright(); //servo right
void servmiddle(); //servo middle
void servleft(); //servo left

void pulse(); //finds distance

char look();
char lookleft();
char lookright();
void BB();
void BR();
// Lab8
void fwdsens();
void navigate();
char pulsar();

int see();
#endif /* PINS_H_ */
