# Lab 7 - "Robot Sensing" Prelab

**Name:** Bret Wagner

**Section:** T3
<br>
<br>
**Documentation:** Mario Bracomonte helped me choose the pins and signals needed
<br>
<br>
<br>
Part I - Understanding the Ultrasonic Sensor and Servo
------------------------------------------------------

#### Ultrasonic Sensor 
1.  How fast does the signal pulse from the sensors travel?
The speed of sound 340m/s
2.  If the distance away from the sensor is 1 in, how long does it take for the
    sensor pulse to return to the sensor?  1 cm?

1 inch: at 13385.8 inches/second: 74.7 microseconds

1 cm: at 340000 cm/second: 29.4 microseconds

3.  What is the range and accuracy of the sensor?

Range: 2cm-4m

Accuracy: up to 3mm

4.  What is the minimum recommended delay cycle (in ms) for using the sensor?  How does this compare to the "working frequency"?

Minimum delay recommended is 60 ms which is 2400 cycles at the 40khz working frequency

    **Going further (optional):**
    Given the max "working frequency", does the maximum sensor range
    make sense?  <u>Hint</u>:  find the maximum unambiguous range.
range = high level time * velocity (340M/S) / 2 
range = 60ms(340/2)=10.2M
??

#### Servo
1.  Fill out the following table identifying the pulse lengths needed for each servo position:

8MHZ Clock

| Servo Position | Pulse Length (ms) | Pulse Length (counts) |
|----------------|:-----------------:|:---------------------:|
| Left           |       1ms         |          8000         |
| Middle         |       1.5ms       |          12000        |
| Right          |       2ms         |          16000        ||
    
<br>

Part II - Using the Ultrasonic Sensor and Servo
-----------------------------------------------

1. Create psuedocode and/or flowchart showing how you will *setup* and *use* the ultrasonic sensor and servo.

		
		Psuedocode
		SetupServo:
			Set up pin 1.1 for PWM
			Set TA0CTL 
		SetupSensor:
			Set up 1.6 as trigger 10ms pule GPIO
			Set up pin 1.3 to recieve the echo
			Set TACCRO for 480000 (60ms cycle)	
		UseServo:
			Set up delays and functions for 
			Left 1ms
			middle 1.5 ms
			right 2ms
			connect with remote
		UseSensor:
			Set up ADC10 to collect analog on sensor (as in class code)	
 			ADC10CTL0 = ADC10SHT_3 + ADC10ON + ADC10IE;
 			ADC10CTL1 = INCH_4;                       
  			ADC10AE0 |= BIT4;                       
			Read TAR Value on rising edge
			Read TAR Value on falling edge
			Subtract to determine time
2. Create a schematic showing how you will setup the ultrasonic sensor and servo.
![Memory Dump](images/schemativ.jpg)
3. Include any other information from this lab you think will be useful in creating your program.  Small snippets from datasheets such as the ultrasonic sensor timing may be good for your report.
![Memory Dump](images/servo.jpg)
![Memory Dump](images/sensor.jpg)	

Helpful code
		void main(void)
		{
		  WDTCTL = WDTPW + WDTHOLD;                 // Stop WDT
		  ADC10CTL0 = ADC10SHT_3 + ADC10ON + ADC10IE; // ADC10ON, interrupt enabled
		  ADC10CTL1 = INCH_4;                       // input A4
		  ADC10AE0 |= BIT4;                         // P1.4 ADC Analog Enable
		  ADC10CTL1 |= ADC10SSEL1|ADC10SSEL0;                // Select SMCLK
		  P1DIR |= 0x01;                            // Set P1.0 to output direction
		
		  while(1)
		  {
		    ADC10CTL0 |= ENC + ADC10SC;             // Sampling and conversion start
		    __bis_SR_register(CPUOFF + GIE);        // LPM0, ADC10_ISR will force exit
		    if (ADC10MEM < 0x1FF)
		      P1OUT &= ~0x01;                       // Clear P1.0 LED off
		    else
		      P1OUT |= 0x01;                        // Set P1.0 LED on
		  }
		}
		
		
		// ADC10 interrupt service routine
		#pragma vector=ADC10_VECTOR
		__interrupt void ADC10_ISR(void)
		{
		  __bic_SR_register_on_exit(CPUOFF);        // Clear CPUOFF bit from 0(SR)
		}

# Lab 8-Robot Maze Prelab

1.  Print out your grading sheet.

2.  Consider your maze navigation strategy.  **Provide pseudocode and/or a flowchart** that shows what your main program loop will do.
   - This should include your collision avoidance algorithm.
		148us per inch, 1MHZ, 150 cts per inch, 450 cts
		
			Psuedocode
			
			For this to work will need to shorten length of fwd(command so it only moves about 2 inches at a time)
			
			front wall
				Checks if a wall is within 3 inches
			
			left wall
				looks left
				checks if a wall is within 3 inches 
			
			Navigate loop
			while(1)
			{
			FWD()			//move
			if( front wall)	//No wall does nothing, new cycle
				{
				}
			else if(left wall) //If wall checks left
				{
				leftturn
				}
			else				//else turn riht
				{
				rightturn
				}
			}
		

		
3.  Include whatever other information from this lab you think will be useful in creating your program.
	-
#### Collision avoidance
How do you avoid hitting a wall?  Below are some questions for your consideration.

 - If your robot does not naturally drive perfectly straight, **how can you correct this "drift"**?
 - 
--increase power to slower wheel, or ad a small turn

 - **How fast does your robot move?**  How far can it move in one full timing cycle?  What is a good way to figure this out?
 
 - not very fast, could test with movement and cycle #'s but not necessary with my plan
 
 - **How fast can you determine "distance"** on each side of your robot (think worst case while moving)?  Should one side have more scanning priority, and how do you determine the priority?
 
 -doesn't really matter robot will stop every time left turn gets priority because of ease of solving both fwd and back

 - If you follow a wall, what is a **good range of distances to attempt to be within?**

	-3-5 inches


# Lab 7 -

## By C2C Bret Wagner

## Table of Contents 
1. [Objectives](#objectives)
2. [Purpose](#purpose)
3. [Prelab](#prelab)
4. [Debugging](#debugging)
5. [Code](#code)
5. [Testing methodology or results](#testing-methodology-or-results)
6. [Observations and Conclusions](#observations-and-conclusions)
7. [Documentation](#documentation)
 
### Objectives
Achieve functionality.
##### Required Functionality
Use the Timer_A subsystem to light LEDs based on the presence of a wall. The presence of a wall on the left side of the robot should light LED1 on your Launchpad board. The presence of a wall next to the right side should light LED2 on your Launchpad board. A wall in front should light both LEDs. Demonstrate that the LEDs do not light until the sensor comes into close proximity with a wall.
##### B Functionality
You need to fully characterize the sensor for your robot. Create a table and graphical plot with at least three data points that shows the rangefinder pulse lengths for a variety of distances from a maze wall to the front/side of your robot. This table/graph should be generated for only one servo position. Use these values to determine how your sensor works so you can properly use it with the servo to solve the maze. How do these experimental values compare to the timing values that you calculated in the Prelab?
##### A Functionality. 
Control your servo position with your remote! Use remote buttons other than those you have used for the motors. Note: you still need to be able to get readings from the ultrasonic sensor after changing your servo position with the remote.
### Purpose
To understand signals and implement them in an interesting way.
### Prelab
---	see above
##### Hardware Considerations
Hardware added includes the Ultrasonic Sensor and the Servo. The servo was controlled by simple PMW. The Ultrasonic was a little more tricky and involved characterization and a pulse.
##### Schematic
![Memory Dump](images/schematic.jpg)

##### Figure 1: Schematic

### Debugging
The biggest issues I had here were hardware based. I had to make heavy use of the lab equipment to make sure the right signals were getting to the right places. Bad wires and loose connections plagued me. 
The biggest code-related issue I has was with the IR sensor control, conflicts with the clock used made it difficult.
### Code
This code comprises the recording for the trigger and echo

			void pulse() //finds distance
		{
		P1DIR |= BIT1;//Mario suggusted I trigger mutiple times so it always hit
		P1OUT |= BIT1;

	    __delay_cycles(80);
		P1OUT &= ~BIT1;

		int see()
	{
		pulse();
		TA0CTL |=	0;		// clear clock
	    TA0CTL |=	(ID_3|TASSEL_2|MC_1|TAIE);		// set clock
		while((P1IN & BIT4)==0)//wait for signal to come in
		TA0R=0x0000;//clear timer
		while((P1IN & BIT4)==BIT4)//wait for signal to end
		kloks = TA0R;
		__delay_cycles(50000);
		return kloks;
	}



	
### Testing methodology and results
Testing was really a step by step process, achieving, get servo working, get US sensor to trigger, adjust trigger, be able to read the echo, map sensor, allow everything to work on the remote.

##### Figure 1:


###Conclusion
This was a very time consuming process and It had me fooled because I thought it would be a quick step from lab 7 to lab 8. However I was very wrong and what worked perfectly fine for my Lab 7 had to be totally redone for lab 8.
https://youtu.be/nCQUIHByJSk 《〈Proof
### Documentation
#####Jake Magness
--Helped me with the prelab logic and choosing pins 
#####Forest Lang
--Helped me get my US sensor blasting and helped me solve conflicts that arose when using the remote.
#####Mario Bracomonte
--Helped me with the trigger and echo setup and some general debugging.










# Lab 8 -

## By C2C Bret Wagner

## Table of Contents 
1. [Objectives](#objectives)
2. [Purpose](#purpose)
3. [Prelab](#prelab)
4. [Debugging](#debugging)
5. [Code](#code)
5. [Testing methodology or results](#testing-methodology-or-results)
6. [Observations and Conclusions](#observations-and-conclusions)
7. [Documentation](#documentation)
 
### Objectives
Achieve functionality. Run it through the maze, and hit no walls! Required: Get to the first gate with only 1 bump
B: get to the second gate
A: Get through gate three without hitting anything
Bonus: Go through backward.
### Purpose
The purpose of this lab was to produce a culminating product that integrated things we learned from the last few labs. It allows cadets to demonstrate and practice all the 382 skills in a neat way
##### Hardware Considerations

##### Schematic
Same as lab 7

###Robot Pic
![Memory Dump](images/bruce.jpg)

Bruce In the Flesh

### Debugging
Going through all of this is just depressing. Hardware errors were there, sensors stopped working, wired stopped connecting, my robot was knocked off a table etc.

My robot wouldn't drive straight or turn a perfect 90degrees so I had to modify the power sent to each motor and for the length of time to make these even.

My robot keeps stopping randomly and I don't know why
### Code

A few important pieces of code were used here
This program moves the robot foreword and stops 2 inches before the wall

			Tvoid fwdsens()
	{
		fr();
		fl();
		TA1CTL &= ~MC_1;
     	TA1CCR1 = 355;
	    TA1CCR2 = 350;
		TA1CTL |= MC_1;
	place = see();
	while(see()>1500)
	{
		//place= see();
		__delay_cycles(100000);
		place = see();
		}	
			TA1CTL &= ~MC_1;
		    TA1CCR1 = Stop;
		    TA1CCR2 = Stop;
			TA1CTL |= MC_1;
	}
This was my loop that the robot followed to go through the maze

        while(1)
        	{
        	servmiddle();
        	fwdsens();
        	if(lookleft())
        	{
               	__delay_cycles(6000);
        		right();
        	}
        	else
        	{
               	__delay_cycles(6000);
        		left();
        	}
        	}
### Testing methodology and results
My methodology was to work through steps, get the robot to stop at the first wall, get it to turn, get it to start etc. Piece by piece until everything is complete. The goes until it gets close to a wall, when it gets to the wall it looks left, it it sees a wall it will turn right and it no wall is present it will turn left and then move foreword again.


###Conclusion
This was the most amount of time I have ever spent on a Lab. It was rewarding to get it to work. I am however very glad that it is over with.

### Documentation
#####Forest Lang
-Made some recommendations for the fine tuning of the robot
#####Mario Bracomonte
Mario has happily looked at my code and made suggestions about why the robot doesn't work well. So far none of them have worked and there are too many to count.
#####Chris Dukarm
Suggested another method of echo collection that I had not considered. It was much simpler than my initial method and so I decided to implement it after 6 hours of failure. So far I have been met with failure with this method as well