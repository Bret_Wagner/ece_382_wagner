#include <msp430.h>
#include "moves.h"


int8	newIrPacket = FALSE;
int16	packetData[32];
int8	packetIndex = 0;
int32   irPacket;
int16	kloks;
int16	kloks2;

void initMSP430() {

	WDTCTL=WDTPW+WDTHOLD; 		// stop WD

	BCSCTL1 = CALBC1_8MHZ;
	DCOCTL = CALDCO_8MHZ;

	P2SEL &= ~BIT6;									// Set up P2.6 as GPIO not XIN
	P2SEL2 &= ~BIT6;									// This action takes
	P2DIR &= ~BIT6;									// three lines of code.
										// Set up P2.6 as GPIO not XIN
										// Once again, this take three lines
										// to properly do

						// check the header out.  P2IES changed.
	P1DIR |= BIT0;
	P1DIR |= BIT6;
	P1OUT &= ~BIT6;
	P1OUT &= ~BIT0;								// And turn the LEDs off

										// create a 16ms roll-over period
	TA0CCR0 = 0x3E80;
    TA0CTL &= ~TAIFG;
	TA0CTL |=	(ID_3|TASSEL_2|MC_1|TAIE);								// clear flag before enabling interrupts = good practice

	P2IFG &= ~BIT6;						// Clear any interrupt flag on P2.3
	P2IE  |= BIT6;						// Enable P2.3 interrupt

	P1DIR &= ~BIT4;	//enable GPI
	P1IN &= ~BIT4;

	HIGH_2_LOW;										// Use 1:8 prescalar off SMCLK and enable interrupts

	__enable_interrupt();
}

//lab7

//void echoklok() {
//
//	WDTCTL=WDTPW+WDTHOLD; 		// stop WD
//
//	BCSCTL1 = CALBC1_8MHZ;
//	DCOCTL = CALDCO_8MHZ;
//
//	P1DIR &= ~BIT4;	//enable GPI
//	P1IN &= ~BIT4; //Mario Saved me here! without this clear I was getting totally inconsistent results
//
//	TA0CCR0 = 0x3E80;
//    TA0CTL &= ~TAIFG;
//	TA0CTL |=	(ID_3|TASSEL_2|MC_1|TAIE);		// clear flag before enabling interrupts = good practice
//
//	LOW_E_HIGH;							// Use 1:8 prescalar off SMCLK and enable interrupts
//	__enable_interrupt();
//}
//lab7

#pragma vector = PORT2_VECTOR			// This is from the MSP430G2553.h file

__interrupt void pinChange (void) {

	int8	pin;
	int16	pulseDuration;			// The timer is 16-bits

	if (IR_PIN)		pin=1;	else pin=0;

	switch (pin) {					// read the current pin level
		case 0:						// !!!!!!!!!NEGATIVE EDGE!!!!!!!!!!
			pulseDuration = TA0R;	//**Note** If you don't specify TA1 or TA0 then TAR defaults to TA0R
			if((pulseDuration <= maxLogic0Pulse) && (pulseDuration >= minLogic0Pulse)){
				irPacket = irPacket << 1;
				irPacket |= 0;
			}

			if((pulseDuration <= maxLogic1Pulse) && (pulseDuration >= minLogic1Pulse)){
							irPacket = irPacket << 1;
							irPacket |= 1;
			}

			TA0CTL = 0;
			LOW_2_HIGH; 				// Set up pin interrupt on positive edge
			break;

		case 1:							// !!!!!!!!POSITIVE EDGE!!!!!!!!!!!
			TA0R = 0x0000;						// time measurements are based at time 0
			TA0CTL |=	(ID_3|TASSEL_2|MC_1|TAIE);
			HIGH_2_LOW; 						// Set up pin interrupt on falling edge
			break;
	} // end switch

	P2IFG &= ~BIT6;			// Clear the interrupt flag to prevent immediate ISR re-entry

} // end pinChange ISR

// -----------------------------------------------------------------------
//			0 half-bit	1 half-bit		TIMER A COUNTS		TIMER A COUNTS
//	Logic 0	xxx
//	Logic 1
//	StarT
//	End
//
// -----------------------------------------------------------------------

//Lab7     ECHO
#pragma vector = PORT1_VECTOR			// This is from the MSP430G2553.h file

//__interrupt void echoChange (void) {
//
//	if (ECHO_PIN)
//	{
//		if(RISE)
//		{
//			TA0CTL = 0;
//			TA0R = 0x0000;
//			TA0CTL |=	(ID_3|TASSEL_2|MC_1|TAIE);
//			HIGH_E_LOW;
//		}
//		else
//		{
//			kloks= TA0R;
//		}
//	}
//
//	P1IFG &= ~BIT4;			// Clear the interrupt flag to prevent immediate ISR re-entry
//
//} // end pinChange ISR
//end lab 7






#pragma vector = TIMER0_A1_VECTOR			// This is from the MSP430G2553.h file
__interrupt void timerOverflow (void)
{

	TA0CTL &= ~TAIFG;
}

//All old above
//All new below
void control(){
	while(1){
		if(irPacket== UP)
		{
			fwdsens();
			irPacket=0;
		}
		if(irPacket== DOWN)
		{
			look();
			irPacket=0;
		}
		if(irPacket== LEFT)
		{
			left();
			irPacket=0;
		}
		if(irPacket== RIGHT)
		{
			navigate();
		    __delay_cycles(Short);
			navigate();
		    __delay_cycles(Short);
		    navigate();
		    __delay_cycles(Short);
		    navigate();
		    __delay_cycles(Short);
		    navigate();
		    __delay_cycles(Short);
			navigate();
		    __delay_cycles(Short);
		    navigate();
		    __delay_cycles(Short);
		    navigate();
		    __delay_cycles(Short);
		    navigate();
		    __delay_cycles(Short);
			navigate();
		    __delay_cycles(Short);
		    navigate();
		    __delay_cycles(Short);
		    navigate();
		    __delay_cycles(Short);
		    navigate();
		    __delay_cycles(Short);
			navigate();
		    __delay_cycles(Short);
		    navigate();
		    __delay_cycles(Short);
		    navigate();
		    __delay_cycles(Short);
		    navigate();
		    __delay_cycles(Short);
			navigate();
		    __delay_cycles(Short);
		    navigate();
		    __delay_cycles(Short);
		    navigate();
		    __delay_cycles(Short);
		    navigate();
		    __delay_cycles(Short);
			navigate();
		    __delay_cycles(Short);
		    navigate();
		    __delay_cycles(Short);
		    navigate();
		    __delay_cycles(Short);
		    navigate();
			irPacket=0;
		}
		if(irPacket== VOL_UP)
		{
			lookright();
			irPacket=0;
		}
		if(irPacket== VOL_DW)
		{
			lookleft();
			irPacket=0;
		}
	}
}
void fr() // foreword left
{
    P2DIR |= BIT2;                // TA1CCR1 on P2.1
    P2SEL |= BIT2;                // TA1CCR1 on P2.1
    P2OUT &= ~BIT2;

    P2DIR |= BIT1;                // TA1CCR1 on P2.2
    P2SEL &= ~BIT1;                // TA1CCR1 on P2.2
    P2OUT |= BIT1;
}
void fl() //foreword right
{
    P2DIR |= BIT4;                // TA1CCR1 on P2.1
    P2SEL |= BIT4;                // TA1CCR1 on P2.1
    P2OUT &= ~BIT4;

    P2DIR |= BIT5;                // TA1CCR1 on P2.1
    P2SEL &= ~BIT5;                // TA1CCR1 on P2.1
    P2OUT |= BIT5;
}
void bl() //backword left
{
    P2DIR |= BIT5;                // TA1CCR1 on P2.1
    P2SEL |= BIT5;                // TA1CCR1 on P2.1
    P2OUT &= ~BIT5;

    P2DIR |= BIT4;                // TA1CCR1 on P2.2
    P2SEL &= ~BIT4;                // TA1CCR1 on P2.2
    P2OUT |= BIT4;
}
void br() //backword right
{
    P2DIR |= BIT1;                // TA1CCR1 on P2.1
    P2SEL |= BIT1;                // TA1CCR1 on P2.1
    P2OUT &= ~BIT1;

    P2DIR |= BIT2;                // TA1CCR1 on P2.1
    P2SEL &= ~BIT2;                // TA1CCR1 on P2.1
    P2OUT |= BIT2;
}
void fwd()
{
	fr();
	fl();

	TA1CTL &= ~MC_1;
    TA1CCR1 = 280;
    TA1CCR2 = Halfp;
	TA1CTL |= MC_1;

    __delay_cycles(Long);
	TA1CTL &= ~MC_1;
    TA1CCR1 = Stop;
    TA1CCR2 = Stop;
	TA1CTL |= MC_1;
}

void fwdsens()
{
	fr();
	fl();

	TA1CTL &= ~MC_1;
    TA1CCR1 = 355;
    TA1CCR2 = 350;
	TA1CTL |= MC_1;
	//see();
//	see();
//	see();
	place = see();
	while(see()>1500)
	{
		//place= see();
		__delay_cycles(100000);
		place = see();

//		place =1;
//		see();
//		if(see()<200){
//				TA1CTL &= ~MC_1;
//			    TA1CCR1 = Stop;
//			    TA1CCR2 = Stop;
//				TA1CTL |= MC_1;
//				place = 0;
//				__delay_cycles(10000);
//		}
//		see();
//		if(see()<200){
//				TA1CTL &= ~MC_1;
//			    TA1CCR1 = Stop;
//			    TA1CCR2 = Stop;
//				TA1CTL |= MC_1;
//				place = 0;
//				__delay_cycles(10000);
		}
		TA1CTL &= ~MC_1;
	    TA1CCR1 = Stop;
	    TA1CCR2 = Stop;
		TA1CTL |= MC_1;

}


void back()
{
	br();
	bl();

	TA1CTL &= ~MC_1;
    TA1CCR1 = Halfp;
    TA1CCR2 = Halfp;
	TA1CTL |= MC_1;

    __delay_cycles(Long);
	TA1CTL &= ~MC_1;
    TA1CCR1 = Stop;
    TA1CCR2 = Stop;
	TA1CTL |= MC_1;
}
void left()
{

	br();
	fl();
	TA1CTL &= ~MC_1;
    TA1CCR1 = Halfp;
    TA1CCR2 = Halfp;
	TA1CTL |= MC_1;

    __delay_cycles(1460000);
	TA1CTL &= ~MC_1;
    TA1CCR1 = Stop;
    TA1CCR2 = Stop;
	TA1CTL |= MC_1;
}
void right()
{
	fr();
	bl();
	TA1CTL &= ~MC_1;
    TA1CCR1 = Halfp;
    TA1CCR2 = Halfp;
	TA1CTL |= MC_1;

    __delay_cycles(1560000);
	TA1CTL &= ~MC_1;
    TA1CCR1 = Stop;
    TA1CCR2 = Stop;
	TA1CTL |= MC_1;
}
void halfleft()
{
	fr();
	bl();
	TA1CTL &= ~MC_1;
    TA1CCR1 = Halfp;
    TA1CCR2 = Halfp;
	TA1CTL |= MC_1;

    __delay_cycles(Short);
	TA1CTL &= ~MC_1;
    TA1CCR1 = Stop;
    TA1CCR2 = Stop;
	TA1CTL |= MC_1;
}
void halfright()
{
	br();
	fl();

	TA1CTL &= ~MC_1;
    TA1CCR1 = Halfp;
    TA1CCR2 = Halfp;
	TA1CTL |= MC_1;

    __delay_cycles(Short);
	TA1CTL &= ~MC_1;
    TA1CCR1 = Stop;
    TA1CCR2 = Stop;
	TA1CTL |= MC_1;
}


// Lab 7
void servright() //servo right
{
	TA0CTL |= TASSEL_2|MC_1|ID_0; //1MHZ
    TA0CCR0 = 20000;//50hz
    TA0CCR1 = 2500;

    TA0CCTL1 |= OUTMOD_7;

	P1DIR |= BIT2;
    P1SEL |= BIT2;

    __delay_cycles(Short);

    P1DIR |= BIT2;                // TA1CCR1 on P2.2
    P1SEL &= ~BIT2;                // TA1CCR1 on P2.2

    initMSP430();

}
void servmiddle() //servo middle
{
	TA0CTL |= TASSEL_2|MC_1|ID_0; //1MHZ
    TA0CCR0 = 20000;//50hz
    TA0CCR1 = 1425;

    TA0CCTL1 |= OUTMOD_7;

	P1DIR |= BIT2;
    P1SEL |= BIT2;

    __delay_cycles(Short);

    P1DIR |= BIT2;                // TA1CCR1 on P2.2
    P1SEL &= ~BIT2;                // TA1CCR1 on P2.2

    initMSP430();

}
void servleft() //servo left
{
	TA0CTL |= TASSEL_2|MC_1|ID_0; //1MHZ
    TA0CCR0 = 20000;//50hz
    TA0CCR1 = 600;

    TA0CCTL1 |= OUTMOD_7;

	P1DIR |= BIT2;
    P1SEL |= BIT2;

    __delay_cycles(Short);

    P1DIR |= BIT2;                // TA1CCR1 on P2.2
    P1SEL &= ~BIT2;                // TA1CCR1 on P2.2

    initMSP430();

}
void pulse() //finds distance
	{
//		echoklok();
		//TA0CTL |= TASSEL_2|MC_1|ID_0; //1MHZ
	//	TA0CCR0 = 20000;//50hz
//		TA0CCR1 = 600;

		P1DIR |= BIT1;//Mario suggusted I trigger mutiple times so it always hit
		P1OUT |= BIT1;

	    __delay_cycles(80);
		P1OUT &= ~BIT1;
//		echoklok();
	}

//char pulsar() //finds distance
//	{
//		__delay_cycles(10000);
//		echoklok();
//		TA0CTL |= TASSEL_2|MC_1|ID_0; //1MHZ
//		TA0CCR0 = 20000;//50hz
//		TA0CCR1 = 600;
//
//		P1DIR |= BIT1;
//		P1OUT |= BIT1;
//	    __delay_cycles(80);
//		P1OUT &= ~BIT1;
//		return kloks;
//	}
//void see();

char look()
{
	servmiddle();
    __delay_cycles(60000);
	see();
	if(kloks<3500)
	{
		BB();
		return 1;
	}
	else
	{
		BR();
		return 0;
	}

}
char lookright()
{
	servleft();
    __delay_cycles(Short);
	see();
	if(kloks<300)
	{
		BL();
		return 1;
	}
	else
	{
		return 0;
	}
}
char lookleft()
{
	servright();
    __delay_cycles(60000);
 //   see();
	if(see()<2000)
	{
		BR();
		return 1;
	}
	else
	{
		BL();
		return 0;
	}
}

void BL() // Blink Left
{
    P1OUT |= BIT6;                     // toggle LED
    __delay_cycles(Long);
	P1OUT &= ~BIT6;
}
void BR() // Blink Right
{
    P1OUT |= BIT0;                     // toggle LED
    __delay_cycles(Long);
	P1OUT &= ~BIT0;
}

void BB() // Blink Right
{
    P1OUT |= BIT0;                     // toggle LED
    P1OUT |= BIT6;                     // toggle LED
    __delay_cycles(Long);
	P1OUT &= ~BIT0;
	P1OUT &= ~BIT6;

}



// Lab8

void navigate()
{
	while(1)
	{
    __delay_cycles(Long);
	if(look())
	{
	    __delay_cycles(Long);

		if(lookleft())
		{
			left();
		    __delay_cycles(Long);
		}
		else
		{
			right();
		    __delay_cycles(Long);
		}
	}
	fwd();
    __delay_cycles(Long);
	}
}

int see()
{
	pulse();
	TA0CTL |=	0;		// clear clock
    TA0CTL |=	(ID_3|TASSEL_2|MC_1|TAIE);		// set clock
	while((P1IN & BIT4)==0)//wait for signal to come in
	TA0R=0x0000;//clear timer
  //kloks2 = TA0R;
	while((P1IN & BIT4)==BIT4)//wait for signal to end
	kloks = TA0R;
//	volatile int mklok= (kloks - kloks2);
	__delay_cycles(50000);
	return kloks;
}
