/*--------------------------------------------------------------------
Name: Bret Wagner
Date: 10/6/16
Course: ECE 382
File: Assignment6.asm
Event: Assignment 6

Purp: A brief description of what this program does and
    the general solution strategy.

Doc:    Chris Dukarm helped me dubugging and general formatting in C

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/
#include <msp430.h> 
#define a 0x38//gloabal constant, notice set up without ";"

/*
 * main.c
 */
int main(void) {
    WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer
    char val1 = 0x40;//declares and assigns values to vals 1-3
    char val2 = 0x35;
    char val3 = 0x42;
    volatile int result1 = 0;//sets up result1 volatile int can change
    volatile int result2 = 0;
    volatile int result3 = 0;
    volatile int i = 0;

    if (val1 > a){			//statement
    	int first =  0;		//declared outside of loop
    	int	second = 1;
    	for(i=0;i<9;i++){	//runs fibinacci to 10 (first # is 0)
    		result1 = first + second;
    		first =  second;
    		second = result1;
    	}
	}

	if(val2 > a)			//value is false, result stays 0
	{
		result2 = 0xAF;
	}
	if(val3 > a)			//triggers
	{
		val2 -=	0x10;
		result3 = val2;
	}
	return 0;
}

main();						//calls function
