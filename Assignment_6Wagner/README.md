###Assignment 6
####Proof
This chart was taken from the debugger and shows all of the variables, the final values for results 1,2, and 3 are listed. This debugging tool was useful because I could also see the value of "i" a variable which was used for my "for loop"

![Memory Dump](images/proof.JPG)
#####Questions
######Exactly where in memory on the microcontroller are your variables stored?
0X03F6-0X03FA
#######How do you know?
Because during my debugging I checked the values tab and it displays the locations as shown in the chart above. 